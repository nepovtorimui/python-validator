FROM alpine:3.6

COPY python-validator.sh /

ENTRYPOINT ["/python-validator.sh"]

